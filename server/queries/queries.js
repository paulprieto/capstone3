const { ApolloServer, gql } = require("apollo-server-express");


// mongoose models. Import your models here.
const Member = require("../models/Member");


// Better to Delete all contents and try to recreate it using your own data.
// Just follow the syntax

const typeDefs = gql`
	# this is a comment
	# the type query is the root of all GraphQL queries
	# this is used for executing GET requests
	
	#define your schemas


	#To make this work you need to create Models and import it above.

	type MemberType {
		id: ID
		firstName: String!
		lastName: String!
		position: String!
		teamID: ID!
		password: String
		username: String
	}

	#define our CRUD
	#R - Retrieve. SYNTAX: 

	type Query {
		#create a your queries and their return value
		
		#sample
		getMembers : [MemberType]

		#retrieving single item/member
		getMember(id: ID! name: String ) : MemberType

	}

	#C - Create, U - update, D - Delete. SYNTAX:

	type Mutation {

		#Sample for create mutation: 
		createMember(
			firstName: String! 
			lastName: String! 
			position: String! 
			teamID: ID 
			username:String! 
			password: String!
		) : MemberType


		#Sample for update mutation:
		updateMember(
			id: ID! 
			firstName: String! 
			lastName: String! 
			position: String! 
			username:String! 
			teamID: ID
			password: String!
		) : MemberType

		#Sample for deleting:
		deleteMember(id: ID!): Boolean


		#Sample for custom mutations:
		logInMember(
			username: String! 
			password: String!
		): MemberType
	}

`;

// Sample resolver
const resolvers = {
	// resolver are what are we going to return when the query is executed

	// Include queries inside Query
	Query : {

		// Sample Query
		getMembers: ()=> {
			return Member.find({})
		}, 

		getMember: (_, args) => {
			return Member.findById(args.id)
		}
	}, 

	// Include mutations inside Mutation
	Mutation: {

		// Sample Mutation
		createMember : (_, args)=> {
			let newMember = new Member({
				firstName: args.firstName,
				lastName: args.lastName,
				position: args.position,
				teamID: args.teamID,
				//Syntax: bcrypt(plain password, salt rounds)
				password: bcrypt.hashSync(args.password, 10),
				username: args.username
			})

			console.log("Creating a member...")
			console.log(args)
			return newMember.save();
		},

		updateMember : (_, args)=> {
			console.log("Updating member info...");
			console.log(args)
			
			let condition = {_id: args.id}
			let updates = {
				firstName: args.firstName,
				lastName: args.lastName,
				position: args.position,
				teamID: args.teamID,
				password: args.password,
				username: args.username
			}

			return Member.findOneAndUpdate(condition,updates)
		},

		deleteMember: (_, args) => {
			console.log("Deleting Member...")
			console.log(args)

			return Member.findByIdAndDelete(args.id).then((member, err)=>{
				console.log(err)
				console.log(member)
				if(err || !member){
					console.log("delete failed.")
					return false
				}

				console.log("Member deleted")
				return true
			})
		}

	},

	// custom resolvers. (you can have many)
	// MemberType : {
	// 	// Sample resolver 
	// 	team : (parent, args) => {
	// 		console.log(parent)
	// 		return Team.findById(parent.teamID)
	// 	}
	// }
}


// Pass the typedefs and resolvers to a variable
// This will be used to communicate with App.js
const server = new ApolloServer({
	typeDefs,
	resolvers
})


module.exports = server;